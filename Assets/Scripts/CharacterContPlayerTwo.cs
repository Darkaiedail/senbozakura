﻿using UnityEngine;
using System.Collections;

public class CharacterContPlayerTwo : MonoBehaviour {

	//if player press some key 
	public bool eKeyDown;
	public bool rKeyDown;

	//animations control
	private Animator anim;

	//manager (button press & scores)
	public GameObject manager;
	InputManager input;
	ScoreManager score;

	//script player one
	public GameObject playerOne;
	CharacterCont contOne;

	//canJoke & deffend images 
	public GameObject firstAttackPlayerTwo;
	public GameObject deffendPlayerOne;

	//reaction time player one
	private float actualTime;
	private float timeDefend;

	//bool to joke or not
	private bool canJoke = true;

	//reaction time player one
	private float timeAttack;
	public bool canAttack;

	//rounds control
	public bool roundOne;
	public bool nextRound;

	void Awake(){

		anim = GetComponent<Animator>();

		input = manager.GetComponent<InputManager>();
		score = manager.GetComponent<ScoreManager>();
		contOne = playerOne.GetComponent<CharacterCont>();
	}

	void Start(){

		roundOne = true;
	}
	
	public void JokeTwo(){

		if(canJoke){

			anim.SetBool("upAttackTwo", false);
			anim.SetBool("downAttackTwo", false);
			anim.SetBool("dieTwo",false);
			anim.SetBool("jokeTwo", true);
			StartCoroutine("finishJoke");
			
			Debug.Log("joke1");
		}
	}
	
	public void ShoutTwo(){

		Debug.Log("shoutTwo");
	}
	
	public void UpAttackTwo(){

		anim.SetBool("jokeTwo", false);
		anim.SetBool("downAttackTwo", false);
		anim.SetBool("dieTwo",false);
		anim.SetBool("upAttackTwo", true);
		StartCoroutine("finishUpAttack");
		
		firstAttack();
		
		Debug.Log("UpAttack_1");
	}
	
	public void DownAttackTwo(){

		anim.SetBool("jokeTwo", false);
		anim.SetBool("upAttackTwo", false);
		anim.SetBool("dieTwo",false);
		anim.SetBool("downAttackTwo", true);
		StartCoroutine("finishDownAttack");
		
		firstAttack();
		
		Debug.Log("DownAttack_1");
	}

	public void Die(){

		anim.SetBool("jokeTwo", false);
		anim.SetBool("upAttackTwo", false);
		anim.SetBool("downAttackTwo", false);
		anim.SetBool("dieTwo",true);

		StartCoroutine("finishDieTwo");
	}
	
	IEnumerator finishJoke(){

		yield return new WaitForSeconds(.46f);
		anim.SetBool("jokeTwo", false);
	}
	
	IEnumerator finishUpAttack(){

		yield return new WaitForSeconds(.6f);
		anim.SetBool("upAttackTwo", false);
	}
	
	IEnumerator finishDownAttack(){

		yield return new WaitForSeconds(.6f);
		anim.SetBool("downAttackTwo", false);
	}

	IEnumerator finishDieTwo(){

		yield return new WaitForSeconds(1.0f);
		anim.SetBool("dieTwo",false);
	}
	
	void firstAttack(){ 
		//Debug.Log("Player 2" + contOne.mutex);
		if (contOne.mutex == false) {

			contOne.mutex = true;

			if (roundOne){

				if(input.upAttackTwo){//E
					
					firstAttackPlayerTwo.SetActive(true);
					deffendPlayerOne.SetActive(true);
					
					//Debug.Log("cacaE");
					eKeyDown = true;
					timeDefend = actualTime;
					
					canJoke = false;
				}
				else if(input.downAttackTwo){//R
					
					firstAttackPlayerTwo.SetActive(true);
					deffendPlayerOne.SetActive(true);
					
					//Debug.Log("cacaR");
					rKeyDown = true;
					timeDefend = actualTime;
					
					canJoke = false;
				}   
			} 
			else if (nextRound){

				if(input.upAttackTwo){//E
					
					//Debug.Log("cacaE");
					eKeyDown = true;
					timeDefend = actualTime;
					
					canJoke = false;

					input.playPlayerOne = true;
				}
				else if(input.downAttackTwo){//R
					
					//Debug.Log("cacaR");
					rKeyDown = true;
					timeDefend = actualTime;
					
					canJoke = false;

					input.playPlayerOne = true;
				}  
			}
		}  
	}
	
	void firstAttackUpAttackTwo(){
		//Debug.Log("PASA POR AQUI");
		if(roundOne){

			if (actualTime <= timeDefend + 1.0f){
				
				if (input.downAttack){//F
					
					firstAttackPlayerTwo.SetActive(false);
					deffendPlayerOne.SetActive(false);
					
					input.downAttack = false;
					input.upAttackTwo = false;
					eKeyDown = false;
					
					score.increaseScoreTwo();
					contOne.Die();
					
					contOne.mutex = false;
					Debug.Log("five");  
					
					canJoke = true;
					roundOne = false;
					contOne.roundOne = false;
					nextRound = true;
					contOne.nextRound = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
				else if(input.upAttack){//D
					
					firstAttackPlayerTwo.SetActive(false);
					deffendPlayerOne.SetActive(false);
					
					input.upAttack = false;
					input.upAttackTwo = false;
					eKeyDown = false;
					
					contOne.mutex = false;
					Debug.Log("six");
					
					canJoke = true;
					roundOne = false;
					contOne.roundOne = false;
					nextRound = true;
					contOne.nextRound = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				} 
			}
			else if (actualTime > timeDefend + 1.0f){
				
				firstAttackPlayerTwo.SetActive(false);
				deffendPlayerOne.SetActive(false);
				
				contOne.Die();
				
				input.upAttackTwo = false;
				eKeyDown = false;
				
				contOne.mutex = false;
				
				canJoke = true;
				roundOne = false;
				contOne.roundOne = false;
				nextRound = true;
				contOne.nextRound = true;

				score.increaseScoreTwo();

				input.startRound = false;
				StartCoroutine("fnishRoundOne");
			}
		} 
		else if(nextRound){

			if (actualTime <= timeDefend + 1.0f){
				
				if (input.downAttack){//F

					firstAttackPlayerTwo.SetActive(false);
					deffendPlayerOne.SetActive(false);
			
					input.downAttack = false;
					input.upAttackTwo = false;
					eKeyDown = false;
					
					score.increaseScoreTwo();
					contOne.Die();
					
					contOne.mutex = false;
					Debug.Log("five");  
					
					canJoke = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
				else if(input.upAttack){//D

					firstAttackPlayerTwo.SetActive(false);
					deffendPlayerOne.SetActive(false);

					input.upAttack = false;
					input.upAttackTwo = false;
					eKeyDown = false;
					
					contOne.mutex = false;
					Debug.Log("six");
					
					canJoke = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				} 
			}
			else if (actualTime > timeDefend + 1.0f){

				firstAttackPlayerTwo.SetActive(false);
				deffendPlayerOne.SetActive(false);

				contOne.Die();
				
				input.upAttackTwo = false;
				eKeyDown = false;
				
				contOne.mutex = false;
				
				canJoke = true;

				score.increaseScoreTwo();

				input.startRound = false;
				StartCoroutine("fnishRoundOne");
			}
		} 
	}
	
	void firstAttackDownAttackTwo(){

		if(roundOne){

			if (actualTime <= timeDefend + 1.0f){
				
				if(input.upAttack){//D
					
					firstAttackPlayerTwo.SetActive(false);
					deffendPlayerOne.SetActive(false);
					
					input.upAttack = false;
					input.downAttackTwo = false;
					rKeyDown = false;
					
					score.increaseScoreTwo();
					contOne.Die();
					
					contOne.mutex = false;
					Debug.Log("seven");
					
					canJoke = true;
					roundOne = false;
					contOne.roundOne = false;
					nextRound = true;
					contOne.nextRound = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
				else if(input.downAttack){//F
					
					firstAttackPlayerTwo.SetActive(false);
					deffendPlayerOne.SetActive(false);
					
					input.downAttack = false;
					input.downAttackTwo = false;
					rKeyDown = false;
					
					contOne.mutex = false;
					Debug.Log("eight");
					
					canJoke = true;
					roundOne = false;
					contOne.roundOne = false;
					nextRound = true;
					contOne.nextRound = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
			}
			else if (actualTime > timeDefend + 1.0f){
				
				firstAttackPlayerTwo.SetActive(false);
				deffendPlayerOne.SetActive(false);
				
				contOne.Die();
				
				input.downAttackTwo = false;
				rKeyDown = false;
				
				contOne.mutex = false;
				
				canJoke = true;
				roundOne = false;
				contOne.roundOne = false;
				nextRound = true;
				contOne.nextRound = true;

				score.increaseScoreTwo();

				input.startRound = false;
				StartCoroutine("fnishRoundOne");
			}
		}
		else if (nextRound){

			if (actualTime <= timeDefend + 1.0f){
				
				if(input.upAttack){//D

					firstAttackPlayerTwo.SetActive(false);
					deffendPlayerOne.SetActive(false);

					input.upAttack = false;
					input.downAttackTwo = false;
					rKeyDown = false;
					
					score.increaseScoreTwo();
					contOne.Die();
					
					contOne.mutex = false;
					Debug.Log("seven");
					
					canJoke = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
				else if(input.downAttack){//F

					firstAttackPlayerTwo.SetActive(false);
					deffendPlayerOne.SetActive(false);

					input.downAttack = false;
					input.downAttackTwo = false;
					rKeyDown = false;
					
					contOne.mutex = false;
					Debug.Log("eight");
					
					canJoke = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
			}
			else if (actualTime > timeDefend + 1.0f){

				firstAttackPlayerTwo.SetActive(false);
				deffendPlayerOne.SetActive(false);

				contOne.Die();
				
				input.downAttackTwo = false;
				rKeyDown = false;
				
				contOne.mutex = false;
				
				canJoke = true;

				score.increaseScoreTwo();

				input.startRound = false;
				StartCoroutine("fnishRoundOne");
			}
		}
	}

	IEnumerator fnishRoundOne(){

		yield return new WaitForSeconds (.5f);

		input.startRound = true;
		input.playPlayerTwo = false;

		contOne.firstAttackPlayerOne.SetActive(true);
		contOne.deffendPlayerTwo.SetActive(true);

		timeAttack = actualTime;
		canAttack = true;
	}
	
	void Update(){

		actualTime = Time.time;
		//Debug.Log("actual" + actualTime);
		//Debug.Log("attack" + timeAttack);

		if(eKeyDown){

			firstAttackUpAttackTwo(); 
		}
		
		else if(rKeyDown){

			firstAttackDownAttackTwo(); 
		}

		if (canAttack){

			if(((actualTime >= (timeAttack + 1.5f)) && !input.downAttackTwo) || 
			   ((actualTime >= (timeAttack + 1.5f)) && !input.upAttackTwo)) {

				input.startRound = false;
				input.playPlayerTwo = true;
				canAttack = false;
				
				//Debug.Log("se te acabo el tiempo jugador dos! " + canAttack);
				
				contOne.firstAttackPlayerOne.SetActive(false);
				contOne.deffendPlayerTwo.SetActive(false);
				
				canJoke = true;

				contOne.StartCoroutine("fnishRoundOne");

				contOne.mutex = false;
			}
		}
	}
}
