﻿using UnityEngine;
using System.Collections;

public class StartButton : MonoBehaviour {
	
	void Update () {
		iTween.ScaleAdd(this.gameObject, iTween.Hash("x", 1.5f,
		                                             "y", 1.5f,
		                                             "time", 1.0f,
		                                             "looptype", "pingPong"));
	}
}
