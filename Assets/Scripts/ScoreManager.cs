﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public int scorePlayerOne = 0;
	public int scorePlayerTwo = 0;

	public Text textOne;
	public Text textTwo;

	public void increaseScoreOne(){

		scorePlayerOne += 1;
	}

	public void increaseScoreTwo(){

		scorePlayerTwo += 1;
	}

	void Update(){

		string scorePlayerOneStr = scorePlayerOne.ToString("00");
		string scorePlayerTwoStr = scorePlayerTwo.ToString("00");

		textOne.text = scorePlayerOneStr;
		textTwo.text = scorePlayerTwoStr;
	}
}
