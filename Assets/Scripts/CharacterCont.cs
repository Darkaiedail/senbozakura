﻿using UnityEngine;
using System.Collections;

public class CharacterCont : MonoBehaviour {

	//if player press some key
	public bool dKeyDown;
	public bool fKeyDown;

	//bool to control the process' order
	public bool mutex;

	//animations control
	private Animator anim;

	//manager (button press & scores)
	public GameObject manager;
	InputManager input;
	ScoreManager score;

	//script player two
	public GameObject playerTwo;
	CharacterContPlayerTwo contTwo;

	//canJoke & deffend images
	public GameObject firstAttackPlayerOne;
	public GameObject deffendPlayerTwo;

	//reaction time player two
	private float actualTime;
	private float timeDeffend;

	//bool to joke or not
	private bool canJoke = true;

	//reaction time player one
	private float timeAttack;
	public bool canAttack;

	//rounds control
	public bool roundOne;
	public bool nextRound;

	void Awake(){

		anim = GetComponent<Animator>();

		input = manager.GetComponent<InputManager>();
		score = manager.GetComponent<ScoreManager>();
		contTwo = playerTwo.GetComponent<CharacterContPlayerTwo>();
	}

	void Start(){

		roundOne = true;
	}
	
	public void Joke(){

		if(canJoke){

			anim.SetBool("upAttack",false);
			anim.SetBool("downAttack",false);
			anim.SetBool("die",false);
			anim.SetBool("joke",true);
			StartCoroutine("finishJoke");
			
			Debug.Log("joke");
		}
	}
	
	public void Shout(){

		Debug.Log("shout");
	}
	
	public void UpAttack(){

		anim.SetBool("joke",false);
		anim.SetBool("downAttack",false);
		anim.SetBool("die",false);
		anim.SetBool("upAttack",true);
		StartCoroutine("finishUpAttack");
		
		firstAttack();
		
		Debug.Log("UpAttack_0");
	}
	
	public void DownAttack(){

		anim.SetBool("joke",false);
		anim.SetBool("upAttack",false);
		anim.SetBool("die",false);
		anim.SetBool("downAttack",true);
		StartCoroutine("finishDownAttack");
		
		firstAttack();
		
		Debug.Log("DownAttack_0");
	}
	
	public void Die(){

		anim.SetBool("joke",false);
		anim.SetBool("upAttack",false);
		anim.SetBool("downAttack",false);
		anim.SetBool("die",true);

		StartCoroutine("finishDie");
	}
	
	IEnumerator finishJoke(){

		yield return new WaitForSeconds(.46f);
		anim.SetBool("joke",false);
	}
	
	IEnumerator finishUpAttack(){

		yield return new WaitForSeconds(.6f);
		anim.SetBool("upAttack",false);
	}
	
	IEnumerator finishDownAttack(){

		yield return new WaitForSeconds(.6f);
		anim.SetBool("downAttack",false);
	}

	IEnumerator finishDie(){

		yield return new WaitForSeconds(1.0f);
		anim.SetBool("die",false);
	}
	
	void firstAttack(){

		if (mutex == false) {
			
			mutex = true;

			if (roundOne){

				if(input.upAttack){//D
					
					firstAttackPlayerOne.SetActive(true);
					deffendPlayerTwo.SetActive(true);
					
					//Debug.Log("cacaD");
					dKeyDown = true;
					//Debug.Log(mutex);
					timeDeffend = actualTime;
					
					canJoke = false;
				}
				else if(input.downAttack){//F
					
					firstAttackPlayerOne.SetActive(true);
					deffendPlayerTwo.SetActive(true);
					
					//Debug.Log("cacaF");
					fKeyDown = true;
					timeDeffend = actualTime;
					
					canJoke = false;
				}
			}
			else if (nextRound){

				if(input.upAttack){//D
					
					//Debug.Log("cacaD");
					dKeyDown = true;
					//Debug.Log(mutex);
					timeDeffend = actualTime;
					
					canJoke = false;
					
					input.playPlayerTwo = true;
				}
				else if(input.downAttack){//F
					
					//Debug.Log("cacaF");
					fKeyDown = true;
					timeDeffend = actualTime;
					
					canJoke = false;
					
					input.playPlayerTwo = true;
				}
			}
		} 
	}
	
	void firstAttackUpAttack(){

		if (roundOne){

			if (actualTime <= timeDeffend + 1.0f){
				
				//Debug.Log("Player 1 " + mutex);
				if (input.downAttackTwo){// R
					
					firstAttackPlayerOne.SetActive(false);
					deffendPlayerTwo.SetActive(false);
					
					input.downAttackTwo = false;
					input.upAttack = false;
					dKeyDown = false;
					
					score.increaseScoreOne();
					contTwo.Die();
					
					mutex = false;
					Debug.Log("one");
					
					canJoke = true;
					roundOne = false;
					contTwo.roundOne = false;
					nextRound = true;
					contTwo.nextRound = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
				else if(input.upAttackTwo){//E
					
					firstAttackPlayerOne.SetActive(false);
					deffendPlayerTwo.SetActive(false);
					
					input.upAttackTwo = false;
					input.upAttack = false;
					dKeyDown = false;
					
					mutex = false;
					Debug.Log("two");
					
					canJoke = true;
					roundOne = false;
					contTwo.roundOne = false;
					nextRound = true;
					contTwo.nextRound = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}  
			}
			else if (actualTime > timeDeffend + 1.0f){
				
				firstAttackPlayerOne.SetActive(false);
				deffendPlayerTwo.SetActive(false);
				
				contTwo.Die();
				
				input.upAttack = false;
				dKeyDown = false;
				
				mutex = false;
				
				canJoke = true;
				roundOne = false;
				contTwo.roundOne = false;
				nextRound = true;
				contTwo.nextRound = true;

				score.increaseScoreOne();

				input.startRound = false;
				StartCoroutine("fnishRoundOne");
			}
		} 
		else if (nextRound){

			if (actualTime <= timeDeffend + 1.0f){
				
				//Debug.Log("Player 1 " + mutex);
				if (input.downAttackTwo){// R

					firstAttackPlayerOne.SetActive(false);
					deffendPlayerTwo.SetActive(false);

					input.downAttackTwo = false;
					input.upAttack = false;
					dKeyDown = false;
					
					score.increaseScoreOne();
					contTwo.Die();
					
					mutex = false;
					Debug.Log("one");
					
					canJoke = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
				else if(input.upAttackTwo){//E

					firstAttackPlayerOne.SetActive(false);
					deffendPlayerTwo.SetActive(false);

					input.upAttackTwo = false;
					input.upAttack = false;
					dKeyDown = false;
					
					mutex = false;
					Debug.Log("two");
					
					canJoke = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}  
			}
			else if (actualTime > timeDeffend + 1.0f){

				firstAttackPlayerOne.SetActive(false);
				deffendPlayerTwo.SetActive(false);

				contTwo.Die();
				
				input.upAttack = false;
				dKeyDown = false;
				
				mutex = false;
				
				canJoke = true;

				score.increaseScoreOne();

				input.startRound = false;
				StartCoroutine("fnishRoundOne");
			}
		}
	}
	
	void firstAttackDownAttack(){

		if(roundOne){

			if (actualTime <= timeDeffend + 1.0f){
				
				if(input.upAttackTwo){//E
					
					firstAttackPlayerOne.SetActive(false);
					deffendPlayerTwo.SetActive(false);
					
					input.upAttackTwo = false;
					input.downAttack = false;
					fKeyDown = false;
					
					score.increaseScoreOne();
					contTwo.Die();
					
					mutex = false;
					Debug.Log("three");
					
					canJoke = true;
					roundOne = false;
					contTwo.roundOne = false;
					nextRound = true;
					contTwo.nextRound = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
				else if(input.downAttackTwo){//R
					
					firstAttackPlayerOne.SetActive(false);
					deffendPlayerTwo.SetActive(false);
					
					input.downAttackTwo = false;
					input.downAttack = false;
					fKeyDown = false;
					
					mutex = false;
					Debug.Log("four"); 
					
					canJoke = true;
					roundOne = false;
					contTwo.roundOne = false;
					nextRound = true;
					contTwo.nextRound = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
			}
			else if (actualTime > timeDeffend + 1.0f){
				
				firstAttackPlayerOne.SetActive(false);
				deffendPlayerTwo.SetActive(false);
				
				contTwo.Die();
				
				input.downAttack = false;
				fKeyDown = false;
				
				mutex = false;
				
				canJoke = true;
				roundOne = false;
				contTwo.roundOne = false;
				nextRound = true;
				contTwo.nextRound = true;

				score.increaseScoreOne();

				input.startRound = false;
				StartCoroutine("fnishRoundOne");
			}
		} 
		else if (nextRound){

			if (actualTime <= timeDeffend + 1.0f){
				
				if(input.upAttackTwo){//E

					firstAttackPlayerOne.SetActive(false);
					deffendPlayerTwo.SetActive(false);

					input.upAttackTwo = false;
					input.downAttack = false;
					fKeyDown = false;
					
					score.increaseScoreOne();
					contTwo.Die();
					
					mutex = false;
					Debug.Log("three");
					
					canJoke = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
				else if(input.downAttackTwo){//R

					firstAttackPlayerOne.SetActive(false);
					deffendPlayerTwo.SetActive(false);

					input.downAttackTwo = false;
					input.downAttack = false;
					fKeyDown = false;
					
					mutex = false;
					Debug.Log("four"); 
					
					canJoke = true;

					input.startRound = false;
					StartCoroutine("fnishRoundOne");
				}
			}
			else if (actualTime > timeDeffend + 1.0f){

				firstAttackPlayerOne.SetActive(false);
				deffendPlayerTwo.SetActive(false);

				contTwo.Die();
				
				input.downAttack = false;
				fKeyDown = false;
				
				mutex = false;
				
				canJoke = true;

				score.increaseScoreOne();

				input.startRound = false;
				StartCoroutine("fnishRoundOne");
			}
		}
	}

	IEnumerator fnishRoundOne(){

		yield return new WaitForSeconds (.5f);
		
		input.startRound = true;
		input.playPlayerOne = false;
		
		contTwo.firstAttackPlayerTwo.SetActive(true);
		contTwo.deffendPlayerOne.SetActive(true);

		timeAttack = Time.time;
		canAttack = true;
	}

	void Update(){

		actualTime = Time.time;
		//Debug.Log("actual" + actualTime);
		//Debug.Log("attack" + timeAttack);

		if(dKeyDown){

			firstAttackUpAttack();
		}
		
		else if(fKeyDown){

			firstAttackDownAttack(); 
		}

		if (canAttack){

			if(((actualTime >= (timeAttack + 1.5f)) && !input.downAttack) || 
			   ((actualTime >= (timeAttack + 1.5f)) && !input.upAttack)) {
				
				input.startRound = false;
				input.playPlayerOne = true;
				canAttack = false;
				
				//Debug.Log("se te acabo el tiempo jugador dos! " + canAttack);
				
				contTwo.firstAttackPlayerTwo.SetActive(false);
				contTwo.deffendPlayerOne.SetActive(false);

				canJoke = true;
			
				contTwo.StartCoroutine("fnishRoundOne");

				mutex = false;
			}
		}
	}
}
