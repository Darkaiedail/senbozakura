﻿using UnityEngine;
using System.Collections;
using System;

public class InputManager : MonoBehaviour{

	public GameObject[]players;
	CharacterCont scriptsPlayerOne;
	CharacterContPlayerTwo scriptsPlayerTwo;
	
	public bool upAttack = false;
	public bool upAttackTwo = false;
	public bool downAttack = false;
	public bool downAttackTwo = false;

	public bool startRound;
	private int startDelay;
	//public GameObject startAdvise;
	public GameObject advisePlayerOne;
	public GameObject advisePlayerTwo;

	public bool playPlayerOne;
	public bool playPlayerTwo;
	
	public AudioSource flaute;
	public AudioSource huyo;
	public AudioSource music;
				
	/* 
	 * Use the update to cycle through the available players, and retrieve the input one by one.
	 * Optionally, you can do it in the FixedUpdate, or you can cache input and process it in the FixedUpdate
	 * if it's meant to be controlling physics.
	 * 
	 */
	
	void Awake(){
		scriptsPlayerOne = players [0].GetComponent<CharacterCont>();
		scriptsPlayerTwo = players [1].GetComponent<CharacterContPlayerTwo>();
	}

	void Start(){
		startDelay = UnityEngine.Random.Range(3,5);
		StartCoroutine("firstRound");

		playPlayerOne = true;
		playPlayerTwo = true;
	}

	IEnumerator firstRound(){
		yield return new WaitForSeconds(startDelay);

		flaute.Stop();

		startRound = true;

		iTween.MoveTo(advisePlayerOne.gameObject, iTween.Hash("x", -13.96f,
		                                                      "y", 9.3f));
		iTween.MoveTo(advisePlayerTwo.gameObject, iTween.Hash("x", 13.96f,
		                                                      "y", 9.3f));
		huyo.Play();
		music.Play();
	}
	
	void Update(){

		if (startRound){

			if(playPlayerOne){

				if(GetButtonDown(0, ButtonMapping.BUTTON_A)|| Input.GetKeyDown(KeyCode.E)){
					scriptsPlayerOne.Joke();
				}
				if(GetButtonDown(0, ButtonMapping.BUTTON_X)){
					scriptsPlayerOne.Shout();
				}
				if(GetButtonDown(0, ButtonMapping.BUTTON_Y)|| Input.GetKeyDown(KeyCode.W)){
					upAttack = true;
					scriptsPlayerOne.UpAttack();
					//Debug.Log("ATACA PLAYER UNO POR ARRIBA PASO A FUNCION FIRST ATTACK");
				}
				if(GetButtonDown(0, ButtonMapping.BUTTON_B)|| Input.GetKeyDown(KeyCode.S)){
					downAttack = true;
					scriptsPlayerOne.DownAttack();
				}
			}
			if(playPlayerTwo){

				if(GetButtonDown(1, ButtonMapping.BUTTON_A) || Input.GetKeyDown(KeyCode.RightArrow)){
					scriptsPlayerTwo.JokeTwo();
				}
				if(GetButtonDown(1, ButtonMapping.BUTTON_X)){
					scriptsPlayerTwo.ShoutTwo();
				}
				if(GetButtonDown(1, ButtonMapping.BUTTON_Y) || Input.GetKeyDown(KeyCode.UpArrow)){
					upAttackTwo = true;
					scriptsPlayerTwo.UpAttackTwo();
				}
				if(GetButtonDown(1, ButtonMapping.BUTTON_B) || Input.GetKeyDown(KeyCode.DownArrow)){
					downAttackTwo = true;
					scriptsPlayerTwo.DownAttackTwo();
				}
			}


		}
	}

	//	/* 
	//	 * Use a function like this to process inputs for each player.
	//	 * You can use Delegates, Actions, or direct function calls, what you prefer!
	//	 * 
	//	 * Just use GetAxisValue() to retrieve an axis value by passing the desired enum (check JoypadMapping file > AxesMapping enum).
	//	 * e.g. AxesMapping.LEFT_X_AXIS for the horizontal axis of the left stick
	//	 * 
	//	 * If you need a normal button, just use GetButtonDown, GetButton, or GetButtonUp and pass the right enum.
	//	 * e.g. ButtonMapping.BUTTON_A for the A button
	//	 * 
	//	 */
	//	private void ProcessJoypadInput(int joyNumber)
	//	{
	//		//Example of how to retrieve the horizontal axis
	//		if(GetAxisValue(joyNumber, AxesMapping.LEFT_X_AXIS) != 0f)
	//		{
	//			//perform action
	//			int readablePlayerNumber = joyNumber + 1;
	//			Debug.Log("Player " + readablePlayerNumber + " is tilting the left stick horizontally by " + GetAxisValue(joyNumber, AxesMapping.LEFT_X_AXIS));
	//		}
	//
	//		//Example of how to retrieve a button
	//		if(GetButtonDown(joyNumber, ButtonMapping.BUTTON_XBOX))
	//		{
	//			//perform action
	//			int readablePlayerNumber = joyNumber + 1;
	//			Debug.Log("Player " + readablePlayerNumber + " pressed the Xbox Button");
	//		}
	//
	//		// The right triggers require a specific treatment on Windows and Mac,
	//		// because on Windows they're mapped to the same axis (the 3rd)
	//
	//#if UNITY_STANDALONE_WIN
	//        if (GetAxisValue(joyNumber, AxesMapping.TRIGGER) < -0.1f)
	//        {
	//        	//perform action
	//        }
	//        if (GetAxisValue(joyNumber, AxesMapping.TRIGGER) > 0.1f)
	//        {
	//            //perform action
	//        }
	//		if (GetButtonDown(joyNumber, ButtonMapping.RIGHT_THUMB_CLICK))
	//        {
	//            //perform action
	//        }
	//#elif UNITY_STANDALONE_OSX
	//		if(GetAxisValue(joyNumber, AxesMapping.RIGHT_TRIGGER) > .1f)
	//		{
	//			//perform action
	//		}
	//		if(GetAxisValue(joyNumber, AxesMapping.LEFT_TRIGGER) > .1f)
	//		{
	//			//perform action
	//		}
	//#endif
	//
	//	}
	
	
	#region AxisHelpers
	public static float GetAxisValue(int playerNumber, AxesMapping axisName)
	{
		return Input.GetAxis(playerNumber + "_Axis" + (int)axisName);
	}
	#endregion
	
	
	
	#region ButtonHelpers
	public static bool GetButtonDown(int playerNumber, ButtonMapping buttonName)
	{
		return Input.GetKeyDown(BToCode(playerNumber, buttonName));
	}
	
	protected bool GetButton(int playerNumber, ButtonMapping buttonName)
	{
		return Input.GetKey(BToCode(playerNumber, buttonName));
	}
	
	protected bool GetButtonUp(int playerNumber, ButtonMapping buttonName)
	{
		return Input.GetKeyUp(BToCode(playerNumber, buttonName));
	}
	
	//converts a ButtonMapping in the right KeyCode enum
	protected static KeyCode BToCode(int playerNumber, ButtonMapping buttonName)
	{
		//This is because for Unity Player0 is the owner of Joypad1
		playerNumber++; //1-based
		
		return (KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + playerNumber + "Button" + (int)buttonName);
	}
	#endregion
}