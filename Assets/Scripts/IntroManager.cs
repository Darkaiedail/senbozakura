﻿using UnityEngine;
using System.Collections;
using System;

public class IntroManager : MonoBehaviour {

	public GameObject startButton;
	public GameObject tittle;

	void Start(){

		StartCoroutine("pressStartButton");
		StartCoroutine("appearTittle");
	}

	IEnumerator appearTittle(){
		
		yield return new WaitForSeconds(.5f);
		tittle.SetActive(true);
	}

	IEnumerator pressStartButton(){

		yield return new WaitForSeconds(1.0f);
		startButton.SetActive(true);
	}

	void Update () {

		if(GetButtonDown(0, ButtonMapping.BUTTON_START) || Input.GetKeyDown(KeyCode.Space)){
			//Debug.Log("AAACCCION!!");
			Application.LoadLevel(1);
		}
		if(GetButtonDown(1, ButtonMapping.BUTTON_START)){

			Application.LoadLevel(1);
		}
	}

	#region AxisHelpers
	public static float GetAxisValue(int playerNumber, AxesMapping axisName)
	{
		return Input.GetAxis(playerNumber + "_Axis" + (int)axisName);
	}
	#endregion
	
	
	
	#region ButtonHelpers
	public static bool GetButtonDown(int playerNumber, ButtonMapping buttonName)
	{
		return Input.GetKeyDown(BToCode(playerNumber, buttonName));
	}
	
	protected bool GetButton(int playerNumber, ButtonMapping buttonName)
	{
		return Input.GetKey(BToCode(playerNumber, buttonName));
	}
	
	protected bool GetButtonUp(int playerNumber, ButtonMapping buttonName)
	{
		return Input.GetKeyUp(BToCode(playerNumber, buttonName));
	}
	
	//converts a ButtonMapping in the right KeyCode enum
	protected static KeyCode BToCode(int playerNumber, ButtonMapping buttonName)
	{
		//This is because for Unity Player0 is the owner of Joypad1
		playerNumber++; //1-based
		
		return (KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + playerNumber + "Button" + (int)buttonName);
	}
	#endregion
}
